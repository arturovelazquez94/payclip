package mx.payclip.commons.persistence.collection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "TransactionClip")
@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TransactionClip {

	@Id
	private String id;

	private String description;

	private BigDecimal amount;

//	private ObjectId userId;

	private Date date;

}
