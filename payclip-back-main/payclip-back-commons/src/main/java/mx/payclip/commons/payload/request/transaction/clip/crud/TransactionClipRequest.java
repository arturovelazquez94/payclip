package mx.payclip.commons.payload.request.transaction.clip.crud;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class TransactionClipRequest {

	private String id;

	private String description;

//	private ObjectId userId;
	private BigDecimal amount;

	private Date date;
}
