
package mx.payclip.commons.exception.handler;


import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
@Primary
@JsonInclude(Include.NON_EMPTY)
public class JSONResponse {

	private String message;

	private Integer error;

	private String errorDescription;

}
