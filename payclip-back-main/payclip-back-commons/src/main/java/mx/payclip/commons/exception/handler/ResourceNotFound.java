package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Deprecated
public class ResourceNotFound extends GenericRestException {
	
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFound(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}
	
	public ResourceNotFound(String message, Integer error) {
		super(message, HttpStatus.NOT_FOUND, error);
	}
	
	public ResourceNotFound(HttpStatus status, String message) {
		super(message, status);
	}
	
}
