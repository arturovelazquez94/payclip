package mx.payclip.commons.payload.response.transaction.clip.crud;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionClipResponse {

	private String id;

	private String description;

	private BigDecimal amount;

//	private ObjectId userId;

	private Date date;

}
