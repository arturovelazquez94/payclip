package mx.payclip.commons.exception.handler;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CreatingResourceException extends GenericRestException {

	private static final String MESSAGE = "Error creating the resource[%s]: %s. Description: %s";

	/**
	 * 
	 */
	private static final long serialVersionUID = -5548545305953309307L;

	public CreatingResourceException(String message) {
		super(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public CreatingResourceException(String resourceId, String description, Class<?> clazz) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId, description), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public CreatingResourceException(HttpStatus httpStatus, String resourceId, String description, Class<?> clazz) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId, description), httpStatus);
	}

}
