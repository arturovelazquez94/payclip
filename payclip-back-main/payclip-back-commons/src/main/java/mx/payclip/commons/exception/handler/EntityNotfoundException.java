package mx.payclip.commons.exception.handler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@EqualsAndHashCode(callSuper=false)
public class EntityNotfoundException extends RuntimeException{
	private static final long serialVersionUID = -8623895923105816997L;
	
	private String codeError;
	private String codeMessage;

}
