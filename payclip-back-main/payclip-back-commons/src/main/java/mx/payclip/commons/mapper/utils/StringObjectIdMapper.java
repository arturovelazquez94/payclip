package mx.payclip.commons.mapper.utils;

import org.bson.types.ObjectId;
import org.mapstruct.Mapper;


@Mapper(componentModel="spring")
public class StringObjectIdMapper {
	public ObjectId asObjectId(String id) {
		return new ObjectId(id);
	}

	public String asString(ObjectId id) {
		return id.toHexString();
	}
}
