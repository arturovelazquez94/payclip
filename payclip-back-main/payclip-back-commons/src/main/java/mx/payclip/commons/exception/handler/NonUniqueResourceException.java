
package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NonUniqueResourceException extends GenericRestException {

	private static final long serialVersionUID = -1865057720402171722L;

	public NonUniqueResourceException(String message) {
		super(message, HttpStatus.CONFLICT);
	}

	public NonUniqueResourceException(String message, Integer error) {
		super(message, HttpStatus.CONFLICT, error);
	}

}
