package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ResourceAlreadyExistException extends GenericRestException  {

	private static final long serialVersionUID = -5774468559457599012L;

	private static final String MESSAGE = "Resource [%s]: %s already exist";

	public ResourceAlreadyExistException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

	public ResourceAlreadyExistException(String resourceId, Class<?> clazz) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId), HttpStatus.BAD_REQUEST);
	}

	public ResourceAlreadyExistException(HttpStatus httpStatus, String resourceId, Class<?> clazz) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId), httpStatus);
	}


}
