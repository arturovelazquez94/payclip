package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;

public class UserException extends GenericRestException {

	private static final long serialVersionUID = 1L;

	public UserException(HttpStatus httpStatus, String message) {
		super(message, httpStatus);
	}

	public UserException(String message) {
		super(message, HttpStatus.CONFLICT);
	}

	public UserException(String message, Integer error) {
		super(message, HttpStatus.CONFLICT, error);
	}
}
