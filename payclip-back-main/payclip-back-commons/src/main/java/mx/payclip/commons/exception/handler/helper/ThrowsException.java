package mx.payclip.commons.exception.handler.helper;


import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;

import mx.payclip.commons.exception.handler.BadRequestException;
import mx.payclip.commons.exception.handler.ConflictResourceException;
import mx.payclip.commons.exception.handler.CreatingResourceException;
import mx.payclip.commons.exception.handler.ResourceAlreadyExistException;
import mx.payclip.commons.exception.handler.ResourceNotFoundException;
import mx.payclip.commons.utils.Nullable;

public class ThrowsException {

	public static <T> T resourceNotFound(Optional<T> resource, String resourceId, Class<T> clazz) {

		if (Nullable.isNull(resource) || !resource.isPresent()) {
			throw new ResourceNotFoundException(resourceId, clazz);
		}

		return clazz.cast(resource.get());
	}

	public static <T> T resourceNotFound(T resource, String resourceId) {

		if (Nullable.isNull(resource)) {
			throw new ResourceNotFoundException(resourceId);
		}

		return resource;
	}

	public static <T> Page<T> resourceNotFound(Page<T> page, String resourceId, Class<T> clazz) {

		if (Nullable.isNull(page)) {
			throw new ResourceNotFoundException(resourceId);
		}

		if (Nullable.isNullOrEmpty(page.getContent())) {
			throw new ResourceNotFoundException(resourceId, clazz);
		}

		return page;
	}

	public static <T> void resourcesNotFound(Collection<String> collection, Class<T> clazz) {

		if (Nullable.isNullOrEmpty(collection)) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		for (String s : collection) {
			sb.append(String.format("%s, ", s));
		}

		throw new ResourceNotFoundException(sb.toString(), clazz);
	}
	
	public static <T> void resourcesNotFound(Collection<T> collection, String resourceId, Class<T> clazz) {

		if (!Nullable.isNullOrEmpty(collection)) {
			return;
		}

		throw new ResourceNotFoundException(resourceId, clazz);
	}

	public static <T> void creatingResource(Optional<T> resource, String resourceId, String description,
			Class<T> clazz) {
		if (resource.isPresent()) {
			throw new CreatingResourceException(resourceId, description, clazz);
		}
	}

	public static <T> void resourceAlreadyExist(Optional<T> resource, String resourceId,
			Class<T> clazz) {
		if (resource.isPresent()) {
			throw new ResourceAlreadyExistException(resourceId, clazz);
		}
	}

	public static <T> void resourceAlreadyExist(T resource, String resourceId,
			Class<T> clazz) {
		if (Nullable.isNotNull(resource)) {
			throw new ResourceAlreadyExistException(resourceId, clazz);
		}
	}
	
	public static <T> void resourceAlreadyExist(Collection<T> resource, String resourceId,
			Class<T> clazz) {
		if (!Nullable.isNullOrEmpty(resource)) {
			throw new ResourceAlreadyExistException(resourceId, clazz);
		}
	}

	public static <T> void conflictResource(String resourceId, Class<T> clazz, String description) {
		throw new ConflictResourceException(resourceId, clazz, description);
	}

	public static void badRequest(String description) {
		throw new BadRequestException(description);
	}



}
