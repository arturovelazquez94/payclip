package mx.payclip.commons.mapper.transaction.clip;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import mx.payclip.commons.mapper.utils.GenericMapper;
import mx.payclip.commons.mapper.utils.StringObjectIdMapper;
import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.clip.crud.TransactionClipResponse;
import mx.payclip.commons.persistence.collection.TransactionClip;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, uses = {
		StringObjectIdMapper.class })
public interface ITransactionClipMapper
		extends GenericMapper<TransactionClip, TransactionClipRequest, TransactionClipResponse> {
}
