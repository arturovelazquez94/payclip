package mx.payclip.commons.exception.handler;


import org.springframework.http.HttpStatus;

public class FileStorageException extends GenericRestException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -949820490739583558L;


	public FileStorageException(String message, Integer error) {
		super(message, HttpStatus.INTERNAL_SERVER_ERROR, error);
	}


	public FileStorageException(String message,  Integer error, Throwable cause) {
		super(message, HttpStatus.INTERNAL_SERVER_ERROR, error, null, cause);
	}

}
