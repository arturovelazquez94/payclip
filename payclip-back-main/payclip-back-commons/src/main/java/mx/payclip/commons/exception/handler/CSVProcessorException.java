package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CSVProcessorException extends GenericRestException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1067498256825000976L;

	public CSVProcessorException(String message) {
		super(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
