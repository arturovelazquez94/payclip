
package mx.payclip.commons.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UpdatingResourceException extends GenericRestException {

	private static final long serialVersionUID = 1L;

	public UpdatingResourceException(String message) {
		super(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public UpdatingResourceException(HttpStatus httpStatus, String message) {
		super(message, httpStatus);

	}

}
