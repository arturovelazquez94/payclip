package mx.payclip.commons.repository.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@Profile("!test")
@Configuration
@EnableMongoRepositories(basePackages = { "mx.ekaax", "mx.ekaax.commons.repository",
		"mx.ekaax.commons.persistence.collection" })
public class MongoConfig extends AbstractMongoConfiguration {

	@Override
	public MongoClient mongoClient() {
		 return new MongoClient("localhost", 27017);
	}

	@Override
	protected String getDatabaseName() {
		return "payclip";
	}

	@Override
	protected String getMappingBasePackage() {
		return "mx.ekaax.commons";
	}

}
