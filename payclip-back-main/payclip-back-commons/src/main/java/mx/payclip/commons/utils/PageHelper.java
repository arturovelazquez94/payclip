package mx.payclip.commons.utils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import mx.payclip.commons.utils.Nullable;

import java.util.ArrayList;
import java.util.List;

public class PageHelper {

	/**
	 * Pagina una lista de objetos
	 *
	 * @param response
	 * @param pageable
	 * @return
	 */
	public static <T> Page<T> createPage(List<T> response, Pageable pageable) {

		int totp = pageable.getPageSize() * (pageable.getPageNumber() + 1);
		Integer max = totp > response.size() ? response.size() : totp;

		return new PageImpl<T>(response.subList(pageable.getPageNumber() * pageable.getPageSize(), max), pageable,
				response.size());
	}

	/**
	 * Crea un objecto Page a partir de una lista ya paginada
	 *
	 * @param response
	 * @param pageable
	 * @param size
	 * @return
	 */
	public static <T> Page<T> createPage(List<T> response, Pageable pageable, Long size) {
		return new PageImpl<T>(Nullable.isNull(response) ? new ArrayList<>() : response, pageable,
				Nullable.isNull(size) ? 0 : size);
	}

}