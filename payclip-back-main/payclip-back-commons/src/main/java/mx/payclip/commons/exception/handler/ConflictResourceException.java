
package mx.payclip.commons.exception.handler;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictResourceException extends GenericRestException {

	private static final long serialVersionUID = 145034924504697274L;

	private static final String MESSAGE = "Conflict in the resource [%s]: %s. Description: %s";

	public ConflictResourceException(String message) {
		super(message, HttpStatus.CONFLICT);
	}

	public ConflictResourceException(String resourceId, Class<?> clazz, String description) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId, description), HttpStatus.CONFLICT);
	}

	public ConflictResourceException(HttpStatus httpStatus, String resourceId, Class<?> clazz, String description) {
		super(String.format(MESSAGE, clazz.getSimpleName(), resourceId, description), httpStatus);
	}

}
