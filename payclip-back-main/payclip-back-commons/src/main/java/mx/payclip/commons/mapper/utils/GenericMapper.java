package mx.payclip.commons.mapper.utils;


import java.util.List;

public interface GenericMapper<E, RQ, RS> {

	RQ entityToRequest(E entity);

	E requestToEntity(RQ request);

	List<E> requestToEntity(List<RQ> request);

	E responseToEntity(RS response);

	List<E> responseToEntity(List<RS> responses);

	RS entityToResponse(E entity);

	List<RS> entityToResponse(List<E> entities);

}
