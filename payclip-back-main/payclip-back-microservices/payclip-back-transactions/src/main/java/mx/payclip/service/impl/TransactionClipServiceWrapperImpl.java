package mx.payclip.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.payclip.commons.mapper.transaction.clip.ITransactionClipMapper;
import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.clip.crud.TransactionClipResponse;
import mx.payclip.commons.persistence.collection.TransactionClip;
import mx.payclip.commons.utils.PageHelper;
import mx.payclip.service.ITransactionClipService;
import mx.payclip.service.ITransactionClipServiceWrapper;

@Service
public class TransactionClipServiceWrapperImpl implements ITransactionClipServiceWrapper {

	@Autowired
	private ITransactionClipMapper transactionClipMapper;

	@Autowired
	private ITransactionClipService transactionClipService;

	@Override
	public TransactionClipResponse findById(String id) {
		return transactionClipMapper.entityToResponse(transactionClipService.findById(id));
	}

	@Override
	public Page<TransactionClipResponse> findAll(Pageable pageable) {
		Page<TransactionClip> transactionsClip = transactionClipService.findAll(pageable);
		List<TransactionClipResponse> transactionsResponse = transactionClipMapper
				.entityToResponse(transactionsClip.getContent());

		return PageHelper.createPage(transactionsResponse, pageable, transactionsClip.getTotalElements());
	}

	@Override
	public TransactionClipResponse create(TransactionClipRequest transaction) {
		TransactionClip transactionEntity = transactionClipMapper.requestToEntity(transaction);
		return transactionClipMapper.entityToResponse(transactionClipService.create(transactionEntity));
	}

	@Override
	public TransactionClipResponse update(TransactionClipRequest transaction, String id) {
		TransactionClip transactionEntity = transactionClipMapper.requestToEntity(transaction);
		return transactionClipMapper.entityToResponse(transactionClipService.update(transactionEntity, id));
	}

	@Override
	public void delete(String id) {
		transactionClipService.delete(id);
	}

}
