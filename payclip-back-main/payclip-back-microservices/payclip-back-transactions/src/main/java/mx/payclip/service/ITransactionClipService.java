package mx.payclip.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.payclip.commons.persistence.collection.TransactionClip;

public interface ITransactionClipService {

	TransactionClip findById(String id);

	Page<TransactionClip> findAll(Pageable pageable);

	TransactionClip create(TransactionClip transaction);

	TransactionClip update(TransactionClip transaction, String id);

	void delete(String id);

}
