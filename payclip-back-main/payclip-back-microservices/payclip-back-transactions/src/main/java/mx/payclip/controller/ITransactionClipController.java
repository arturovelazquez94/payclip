package mx.payclip.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.clip.crud.TransactionClipResponse;

@RestController
@RequestMapping("/")
public interface ITransactionClipController {

	@GetMapping("{id}")
	ResponseEntity<TransactionClipResponse> getById(String id);

	@GetMapping
	ResponseEntity<Page<TransactionClipResponse>> getAll(Pageable pageable);

	@PostMapping
	ResponseEntity<TransactionClipResponse> create(TransactionClipRequest transactionRequest);

	@PutMapping("{id}")
	ResponseEntity<TransactionClipResponse> update(TransactionClipRequest transactionRequest, String id);

	@DeleteMapping("{id}")
	ResponseEntity<Void> delete(String id);

}
