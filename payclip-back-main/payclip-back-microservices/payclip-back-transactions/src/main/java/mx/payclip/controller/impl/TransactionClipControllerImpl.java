package mx.payclip.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.clip.crud.TransactionClipResponse;
import mx.payclip.controller.ITransactionClipController;
import mx.payclip.service.ITransactionClipServiceWrapper;

@Component
public class TransactionClipControllerImpl implements ITransactionClipController {

	@Autowired
	private ITransactionClipServiceWrapper transactionClipServiceWrapper;

	@Override
	public ResponseEntity<TransactionClipResponse> getById(@PathVariable String id) {
		return ResponseEntity.ok(transactionClipServiceWrapper.findById(id));
	}

	@Override
	public ResponseEntity<Page<TransactionClipResponse>> getAll(Pageable pageable) {
		return ResponseEntity.ok(transactionClipServiceWrapper.findAll(pageable));
	}

	@Override
	public ResponseEntity<TransactionClipResponse> create(@RequestBody TransactionClipRequest transactionRequest) {
		return ResponseEntity.ok(transactionClipServiceWrapper.create(transactionRequest));
	}

	@Override
	public ResponseEntity<TransactionClipResponse> update(@RequestBody TransactionClipRequest transactionRequest,
			@PathVariable String id) {
		return ResponseEntity.ok(transactionClipServiceWrapper.update(transactionRequest, id));
	}

	@Override
	public ResponseEntity<Void> delete(@PathVariable String id) {
		transactionClipServiceWrapper.delete(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

}
