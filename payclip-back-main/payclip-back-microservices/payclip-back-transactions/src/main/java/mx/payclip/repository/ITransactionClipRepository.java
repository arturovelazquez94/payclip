package mx.payclip.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.payclip.commons.persistence.collection.TransactionClip;

@Repository
public interface ITransactionClipRepository extends MongoRepository<TransactionClip, String> {

}
