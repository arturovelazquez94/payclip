package mx.payclip.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.clip.crud.TransactionClipResponse;

public interface ITransactionClipServiceWrapper {

	TransactionClipResponse findById(String id);

	Page<TransactionClipResponse> findAll(Pageable pageable);

	TransactionClipResponse create(TransactionClipRequest transaction);

	TransactionClipResponse update(TransactionClipRequest transaction, String id);

	void delete(String id);
}
