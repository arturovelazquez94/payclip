package mx.payclip.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.payclip.commons.exception.handler.helper.ThrowsException;
import mx.payclip.commons.persistence.collection.TransactionClip;
import mx.payclip.helper.TransactionClipHelper;
import mx.payclip.repository.ITransactionClipRepository;
import mx.payclip.service.ITransactionClipService;

@Service
public class TransactionClipServiceImpl implements ITransactionClipService {

	@Autowired
	private ITransactionClipRepository transactionClipRepository;

	@Override
	public TransactionClip findById(String id) {
		Optional<TransactionClip> transactionClip = transactionClipRepository.findById(id);
		return ThrowsException.resourceNotFound(transactionClip, id, TransactionClip.class);
	}

	@Override
	public Page<TransactionClip> findAll(Pageable pageable) {
		return transactionClipRepository.findAll(pageable);
	}

	@Override
	public TransactionClip create(TransactionClip transaction) {
		return transactionClipRepository.insert(transaction);
	}

	@Override
	public TransactionClip update(TransactionClip transaction, String id) {

		TransactionClip transactionClip = transactionClipRepository.findById(id).orElse(null);
		ThrowsException.resourceNotFound(Optional.ofNullable(transactionClip), id, TransactionClip.class);

		TransactionClipHelper.setUpdateProperties(transactionClip, transaction);

		transactionClipRepository.save(transactionClip);
		return transactionClip;
	}

	@Override
	public void delete(String id) {
		TransactionClip transactionClip = transactionClipRepository.findById(id).orElse(null);
		ThrowsException.resourceNotFound(Optional.ofNullable(transactionClip), id, TransactionClip.class);

		transactionClipRepository.deleteById(id);
	}

}
