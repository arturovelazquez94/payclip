package mx.payclip.helper;

import mx.payclip.commons.persistence.collection.TransactionClip;
import mx.payclip.commons.utils.Nullable;

public class TransactionClipHelper {

	public static void setUpdateProperties(TransactionClip entity, TransactionClip newEntity) {

		if (Nullable.isNotNull(newEntity)) {

			if (Nullable.isNotNull(newEntity.getDescription())) {

				entity.setDescription(newEntity.getDescription());
			}
			if (Nullable.isNotNull(newEntity.getAmount())) {

				entity.setAmount(newEntity.getAmount());
			}
			if (Nullable.isNotNull(newEntity.getDate())) {

				entity.setDate(newEntity.getDate());
			}
		}
	}

}
